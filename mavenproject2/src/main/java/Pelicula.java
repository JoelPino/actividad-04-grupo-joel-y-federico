
public class Pelicula extends Filmografia
{
    public Pelicula (String titulo, String genero, String año)
    {
        this.titulo = titulo;
        this.genero = genero;
        this.año = año;
    }
    public String toString ()
        {
            return "//" + this.titulo + "//" + this.genero + "//" + this.año;
        }
}
