
public class Serie extends Filmografia{
    String temporada; 
    public Serie (String titulo, String genero, String año,String temporada)
    {
        this.titulo = titulo;
        this.genero = genero;
        this.año = año;
        this.temporada = temporada; 
    }
    @Override
   public String toString ()
        {
            return "//" + this.titulo + "//" + this.genero + "//" + this.año + "//" + this.temporada;
        }
}
