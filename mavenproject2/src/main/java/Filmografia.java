public abstract class Filmografia {
    String titulo;
    String genero;
    String año;
    
    public void ObtenerTitulo(String titulo)
    {
        this.titulo = titulo;
    }
    public void ObtenerGenero(String genero)
    {
        this.genero = genero;
    }
    public void ObtenerAño(String año)
    {
        this.año = año;
    }
}
